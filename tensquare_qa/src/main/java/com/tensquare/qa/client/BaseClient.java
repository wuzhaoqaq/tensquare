package com.tensquare.qa.client;

import com.tensquare.qa.impl.BaseClientImpl;
import entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * feign实现服务之间的调用
 */
@Component
@FeignClient(value = "tensquare-base",fallback = BaseClientImpl.class) // 开启熔断器
public interface BaseClient {
    /**
     * 问题服务调用基础服务
     * 调用base微服务中的findById方法
     * @param labelId
     * @return
     */
    @RequestMapping(value = "/label/{labelId}", method = RequestMethod.GET)
    public Result findById(@PathVariable("labelId") String labelId) ;

}
