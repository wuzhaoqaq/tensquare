package com.tensquare.qa.dao;

import com.tensquare.qa.pojo.Problem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ProblemDao extends JpaRepository<Problem,String>,JpaSpecificationExecutor<Problem>{
    /**
     * 最新问题列表
     * nativeQuery 写原生的sql语句
     * sql语句两种写法，尽量少用in，这样SQL执行效率不高
     * SELECT * FROM tb_problem,tb_pl WHERE id =problemid AND labelid="1" ORDER BY replytime DESC   根据回复时间最新排序
     * SELECT * FROM tb_problem WHERE id IN (SELECT problemid FROM tb_pl WHERE labelid="1")
     * 总结：SpringDataJpa查询用的最多的是：原生SQL
     *      或者jpql语句   通过注解@Query("") 默认支持HQL语句
     *      以后通过命名规则查询   例如：findByUsernameAndPassword(String user, Striang pwd)； 关键字：And
     *
     * @return
     */
    @Query(value ="SELECT * FROM tb_problem,tb_pl WHERE id =problemid AND labelid=? ORDER BY replytime DESC",nativeQuery = true)
    public Page<Problem> newlist(String labelid, Pageable pageable);

    /**
     * 热门问题列表
     * @return
     */
    @Query(value ="SELECT * FROM tb_problem,tb_pl WHERE id =problemid AND labelid=? ORDER BY reply DESC",nativeQuery = true)
    public Page<Problem> hotist(String labelid, Pageable pageable);

    /**
     * 等待问题列表
     * @return
     */
    @Query(value ="SELECT * FROM tb_problem,tb_pl WHERE id =problemid AND labelid=? AND reply=0 ORDER BY createtime DESC",nativeQuery = true)
    public Page<Problem> waitlist(String labelid, Pageable pageable);

	
}
