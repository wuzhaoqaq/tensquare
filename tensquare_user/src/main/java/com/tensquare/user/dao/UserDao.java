package com.tensquare.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tensquare.user.pojo.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 * @author Administrator
 * JpaRepository<User,String>,JpaSpecificationExecutor<User> 这两个接口配合使用，开发中用的最多
 */
public interface UserDao extends JpaRepository<User,String>,JpaSpecificationExecutor<User>{

    public User findByMobile(String mobile);

    @Modifying //注意加上这个主键 才能实现增删改查
    @Query(value = "update tb_user set fanscount=fanscount+? where id=?",nativeQuery = true)
    public void updateFanscount(int x, String friendid);

    @Modifying
    @Query(value = "update tb_user set followcount=followcount+? where id=?",nativeQuery = true)
    public void updateFollowcount(int x, String userid);

}
