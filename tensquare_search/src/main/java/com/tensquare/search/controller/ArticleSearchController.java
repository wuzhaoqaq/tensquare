package com.tensquare.search.controller;


import com.tensquare.search.pojo.Article;
import com.tensquare.search.service.ArticleSearchService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/article")
public class ArticleSearchController {

    @Autowired
    private ArticleSearchService articleSearchService;

    /**
     * 文章报错到索引库
     * @param article
     * @return
     */
    @RequestMapping(method= RequestMethod.POST)
    public Result save(@RequestBody Article article){
        articleSearchService.save(article);
        return new Result(true, StatusCode.OK, "添加成功");
    }

    /**
     * 搜索
     * @param key  关键字
     * @param page
     * @param age
     * @return
     */
    @RequestMapping(value="/{key}/{page}/{size}",method= RequestMethod.GET)
    public Result findByKey(@PathVariable String key,@PathVariable int page,@PathVariable int age,@PathVariable int size){

        Page<Article> pageData = articleSearchService.findByKey(key,page,size);
        return new Result(true, StatusCode.OK, "搜索成功",new PageResult<Article>(pageData.getTotalElements(),pageData.getContent()));

    }


}
