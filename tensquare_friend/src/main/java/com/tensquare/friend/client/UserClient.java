package com.tensquare.friend.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 交友微服务调用用户微服务  客户端
 */
@FeignClient("tensquare-user")
public interface UserClient {

    /**
     * 更新关注数
     *    粉丝数
     * @param userid
     * @param friendid
     * @param x
     */
    @RequestMapping(value = "/user/{userid}/{friendid}/{x}",method = RequestMethod.PUT)
    public void updateFanscountAndFollowcount(@PathVariable("userid") String userid, @PathVariable ("friendid")String friendid, @PathVariable("x") int x);

}
